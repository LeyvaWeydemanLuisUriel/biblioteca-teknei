git clone https://gitlab.com/LeyvaWeydemanLuisUriel/biblioteca-teknei.git

--BACKEND
-Tener instalado composer, laravel 9, php 8.1.6
-Crear la base de datos backend en xampp/mysql
-En la terminal ubicarse en C:\......\backend
-Ejecutar composer install
-Copiar .env.example y reenombrarlo en .env
-Abra su archivo .env y cambie el nombre de la base de datos ( DB_DATABASE) a lo que tenga, el campo de nombre de usuario ( DB_USERNAME) y contraseña ( DB_PASSWORD) corresponden a su configuración de xampp
-Ejecutar php artisan key:generate
-Ejecutar php artisan migrate
-Ejecutar php artisan serve

FRONTEND
-En la terminal ubicarse en C:\......\frontend
-Ejecutar npm install, para las depedencias
-ejecutar npm run ng serve


-Validaciones
-Campos requeridos
 *LIBROS
 -Titulo -> Requerido y longitud menor a  50.
 -Autor -> Requerido y longitud menor a  50.
 -Categoria -> Requerida.
 -Portada -> Requerido y archivo.

 *CATEGORIA
 -Nombre -> Requerido y longitud menor a  20.
 -Descripcion -> Requerido y longitud menor a  100.

 -----Al editar el libro no se puede cambiar de categoria.
