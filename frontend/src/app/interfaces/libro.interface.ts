export interface Libro{
    id?:number,
    autor?:string,
    titulo?:string,
    portada?:string,
    categorias_id?:number,
    categoria:string
}