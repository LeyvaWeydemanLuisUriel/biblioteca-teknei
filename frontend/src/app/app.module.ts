import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { CategoriaListaComponent } from './components/categorias/categoria-lista/categoria-lista.component';
import { CategoriaAgregarComponent } from './components/categorias/categoria-agregar/categoria-agregar.component';
import { CategoriaEditarComponent } from './components/categorias/categoria-editar/categoria-editar.component';
import { CategoriaFormularioComponent } from './components/categorias/categoria-formulario/categoria-formulario.component';
import { LibroFormularioComponent } from './components/libros/libro-formulario/libro-formulario.component';
import { LibroEditarComponent } from './components/libros/libro-editar/libro-editar.component';
import { LibroAgregarComponent } from './components/libros/libro-agregar/libro-agregar.component';
import { LibroListaComponent } from './components/libros/libro-lista/libro-lista.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbActiveModal, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LibroVerComponent } from './components/libros/libro-ver/libro-ver.component';
import { CategoriaVerComponent } from './components/categorias/categoria-ver/categoria-ver.component';

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    NavbarComponent,
    CategoriaListaComponent,
    CategoriaAgregarComponent,
    CategoriaEditarComponent,
    CategoriaFormularioComponent,
    LibroFormularioComponent,
    LibroEditarComponent,
    LibroAgregarComponent,
    LibroListaComponent,
    LibroVerComponent,
    CategoriaVerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    NgbModule,
    NgbModalModule,
    FlexLayoutModule,
  
  ],
  
  providers: [NgbActiveModal],
  bootstrap: [AppComponent]
})
export class AppModule { }
