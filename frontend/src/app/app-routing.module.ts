import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriaListaComponent } from './components/categorias/categoria-lista/categoria-lista.component';
import { LibroListaComponent } from './components/libros/libro-lista/libro-lista.component';

const routes: Routes = [
  {path:'', component:LibroListaComponent},
  {path:'libros', component:LibroListaComponent},
  {path:'categorias', component:CategoriaListaComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
