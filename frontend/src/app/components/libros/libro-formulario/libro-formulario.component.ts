import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Libro } from 'src/app/interfaces/libro.interface';
import { CategoriaService } from 'src/app/services/categoria.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-libro-formulario',
  templateUrl: './libro-formulario.component.html',
  styleUrls: ['./libro-formulario.component.css']
})
export class LibroFormularioComponent implements OnInit {
  @Input() libro: Libro;
  loading = false;
  @Output() submitForm: EventEmitter<any> = new EventEmitter();
  public libroForm: FormGroup;
  public submit: boolean = false;
  categoriasLista: any[] = [];
  libroCategoria:any;
  archivo: File;
  portadaPlaceHolder: any;
  idCategoria:any;
  constructor( private fb: FormBuilder,
    private categoriaService: CategoriaService) { }

  ngOnInit(): void {
    this.loading = true;

    if (this.libro) {
      this.categoriaService
        .getCategoria(this.libro.categorias_id)
        .subscribe((data: any) => {
          this.libroCategoria=(data);
          this.loading = false;
        });
     
    } else {
      this.categoriaService.getCategorias().subscribe((data: any) => {
        this.categoriasLista = data;
        this.loading = false;
      });
    }

    this.portadaPlaceHolder = '';

    this.crearFormulario();
    this.setValores();
  }
  onSelectionChange(event: any) {
    this.idCategoria = event.target.value;
  }

  crearFormulario(): void {
    this.libroForm = this.fb.group({
      id: '',
      titulo: ['', [Validators.required,Validators.maxLength(50)]],
      autor: ['',[Validators.required,Validators.maxLength(50)]],
      categorias_id: ['', Validators.required],
      portada: ['',Validators.required],
    });
  }
  get f(): any {
    return this.libroForm.controls;
  }
  onSubmit(): void {
    this.submit = true;

    if (this.libroForm.invalid) {
      Swal.fire('Complete los datos requeridos', '', 'error');
      window.scrollTo(0, 0);
    } else {
      let datos = new FormData();
      datos.append(
        'titulo',
         this.libroForm.get('titulo')?.value
      );
      datos.append(
        'autor',
         this.libroForm.get('autor')?.value
      );
      datos.append(
        'categorias_id',
        this.idCategoria
      );

      datos.append('portada', this.archivo);

      this.submitForm.emit(datos);
    }
  }

  uploadFile(event: any): void {
    let elem = event.target;

    if (elem.files.length > 0) {
      this.archivo = event.target.files[0];
      this.portadaPlaceHolder = this.archivo.name;

    } 
  }

  private setValores(): void {
    if (this.libro) {
      let { id, titulo, autor, categorias_id, portada } = this.libro;
      
      this.libroForm.patchValue({
        id,
        titulo,
        autor,
        categorias_id,
        portada,
      });

      this.portadaPlaceHolder = portada;
    }
  }

  borrarFormulario(): void {
    this.libroForm.reset();
    this.archivo = null;
    this.portadaPlaceHolder = '';
    this.submit = false;
  }

  getName(fullPath){
    return fullPath.replace(/^.*[\\\/]/, '');
  }

}
