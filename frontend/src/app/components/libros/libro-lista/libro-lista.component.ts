import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { map, Observable, startWith } from 'rxjs';
import { Libro } from 'src/app/interfaces/libro.interface';
import { LibroService } from 'src/app/services/libro.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'
import { LibroAgregarComponent } from '../libro-agregar/libro-agregar.component';

@Component({
  selector: 'app-libro-lista',
  templateUrl: './libro-lista.component.html',
  styleUrls: ['./libro-lista.component.css']
})
export class LibroListaComponent implements OnInit {
  libro:Libro;
  libros$: Observable<Libro[]>;
  filter = new FormControl('');
  dataTable = [];
  loading = false;
  closeResult = '';
  ruta = environment.ruta;
  constructor(
    private libroService:LibroService,
    public modalService: NgbModal
  ) {
       
  }


  ngOnInit(): void {
    this.loadLibros();
    
  }
  search(text: string): Libro[] {
    return this.dataTable.filter((libro:any) => {
      const term = text.toLowerCase();
      return libro.categoria.toLowerCase().includes(term)
          
    });
  }
  agregarInformacion() {
    this.modalService.open(LibroAgregarComponent).result.then((result) => {
      console.log(result);
      if(result==null){
        this.loadLibros();
      }
    }, (reason) => {
      console.log(reason);
    });
    

  }

  loadLibros() {
    this.loading = true;
    this.libroService
      .getLibros()
      .subscribe((data) => {
        console.log(data)

        if (data.length != 0) {
          this.dataTable = data;
          console.log(this.dataTable)
          this.libros$ = this.filter.valueChanges.pipe(
            startWith(''),
            map(text => this.search(text))
          );
          this.loading = false;
        } else {
          
          this.loading = false;

          Swal.fire({
            title:
              'Sin libros registrados',
            text: '',
            icon: 'error',
            allowOutsideClick: false,
          }).then((r) => {
          });
        }
      });
  }
 
  editar(template: TemplateRef<any>,lib:Libro) {
    this.libro = lib;

    this.modalService.open(template).result.then((result) => {
      if(!result){
        this.loadLibros();

      }
    }, (reason) => {
      if(!reason)
      this.loadLibros();

    });
   
  }

  ver(template: TemplateRef<any>,lib:Libro){

    this.libro = lib;
    this.modalService.open(template).result.then((result) => {
      if(!result){
        this.loadLibros();
      }
    }, (reason) => {
      if(!reason)
      this.loadLibros();

    });

  }

  borrar(element) {
    Swal.fire({
      title: '¿Estás seguro de eliminar el libro ' + element.titulo + '?',
      text: '¡Esta acción no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.loading = true;
        this.libroService
          .deleteLibro(element.id)
          .subscribe((data) => {
            if (data.message == 'Eliminado') {
              this.loading = false;
              Swal.fire({
                title: 'Registro eliminado',
                text: 'Has eliminado el registro correctamente',
                icon: 'success',
                allowOutsideClick: false,
              }).then((result) => {
                if (result.isConfirmed) {
                 this.loadLibros();
                }
              });

              //this.loading = false;
            } else {
              this.loading = false;

              Swal.fire({
                title: 'No pudo eliminarse',
                text: '',
                icon: 'error',
                allowOutsideClick: false,
              });
            }
          });
      }
    });
  }
 

}
