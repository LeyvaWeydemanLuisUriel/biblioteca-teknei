import { Component, Input, OnInit } from '@angular/core';
import { Libro } from 'src/app/interfaces/libro.interface';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-libro-ver',
  templateUrl: './libro-ver.component.html',
  styleUrls: ['./libro-ver.component.css']
})
export class LibroVerComponent implements OnInit {

  @Input() libro:Libro;
  @Input() modal:any;
  ruta = environment.ruta;

  constructor() { }

  ngOnInit(): void {
  }
  
  close(){
    this.modal.dismissAll()
  }

}
