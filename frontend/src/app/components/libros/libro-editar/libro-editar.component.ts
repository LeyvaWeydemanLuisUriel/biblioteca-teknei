import { Component, Input, OnInit } from '@angular/core';
import { Libro } from 'src/app/interfaces/libro.interface';
import { LibroService } from 'src/app/services/libro.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-libro-editar',
  templateUrl: './libro-editar.component.html',
  styleUrls: ['./libro-editar.component.css']
})
export class LibroEditarComponent implements OnInit {

  @Input() libroInput:any;
  @Input() modal:any;

  libro: Libro;
  loading = false;
  constructor(
    private libroService: LibroService
  ) {
    
  }

  ngOnInit(): void {
    this.loading = true;
    this.libroService.getLibro(this.libroInput.id).subscribe((data) => {
      if (data) {
        this.libro = data;

        this.loading = false;

        //  console.log(this.obligaciones)
      }
      this.loading = false;
    });
  }
  
  crear(libro: Libro): void {
    this.loading = true;

    this.libroService
      .updateLibro(this.libroInput.id, libro)
      .subscribe((data: any) => {
        this.libro = data;
        //console.log(valor)
        this.loading = false;
        if (data) {
          Swal.fire({
            title: 'Información actualizada',
            text: '',
            icon: 'success',
            allowOutsideClick: false,
          }).then((r) => {
            this.closeModal();
            //window.location.reload();
           // this.dialogRef.close(data);
          });
        } else {
          Swal.fire({
            title: 'No se pudo actualizar la información',
            text: 'Revisa los campos',
            icon: 'error',
            allowOutsideClick: false,
          }).then((r) => {});
        }
      });
  }


  closeModal(){

    this.modal.dismissAll();
  }

}
