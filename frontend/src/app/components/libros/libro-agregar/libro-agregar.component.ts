import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Libro } from 'src/app/interfaces/libro.interface';
import { LibroService } from 'src/app/services/libro.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-libro-agregar',
  templateUrl: './libro-agregar.component.html',
  styleUrls: ['./libro-agregar.component.css']
})
export class LibroAgregarComponent implements OnInit {
  loading = false;
  constructor(private libroService:LibroService ,public activeModal: NgbActiveModal) { 
    
  }

  ngOnInit(): void {
  }
  crear(libro:Libro): void {
    //console.log(libro);
    this.loading=true;
    this.libroService.addLibro(libro).subscribe((data:any) =>{
       // console.log(data)
        if(data){
          //this.loading=false;

          Swal.fire({title:'Información guardada con éxito.',text:'',icon:'success',allowOutsideClick:false}).then(
            ()=>{
              this.closeModal(); 

            }
          )
          this.loading=false;

                }else{
                 // this.loading=false;
                 this.loading=false;

          Swal.fire({title:'No se pudo guardar la información.',text:'',icon:'error',allowOutsideClick:false})

        }
      })
   

  }
  closeModal() {
    this.activeModal.close(null);
  }

}
