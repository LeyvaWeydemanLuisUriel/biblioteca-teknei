import { Component, Input, OnInit } from '@angular/core';
import { Categoria } from 'src/app/interfaces/categoria.interface';

@Component({
  selector: 'app-categoria-ver',
  templateUrl: './categoria-ver.component.html',
  styleUrls: ['./categoria-ver.component.css']
})
export class CategoriaVerComponent implements OnInit {
  @Input() categoria:Categoria;
  @Input() modal:any;

  constructor() { }

  ngOnInit(): void {
  }
  
  close(){
    this.modal.dismissAll()
  }

}
