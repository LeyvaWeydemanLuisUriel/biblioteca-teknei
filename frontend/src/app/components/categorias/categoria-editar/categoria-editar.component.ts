import { Component, Input, OnInit } from '@angular/core';
import { Categoria } from 'src/app/interfaces/categoria.interface';
import { CategoriaService } from 'src/app/services/categoria.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-categoria-editar',
  templateUrl: './categoria-editar.component.html',
  styleUrls: ['./categoria-editar.component.css']
})
export class CategoriaEditarComponent implements OnInit {

  @Input() categoriaInput:any;
  @Input() modal:any;

  categoria: Categoria;
  loading = false;
  constructor(
    private categoriaService: CategoriaService
  ) {
    
  }

  ngOnInit(): void {
    this.loading = true;

    this.categoriaService.getCategoria(this.categoriaInput.id).subscribe((data) => {
      if (data) {
        this.categoria = data;

        this.loading = false;

        //  console.log(this.obligaciones)
      }
      this.loading = false;
    });
  }
  
  crear(categoria: Categoria): void {
    this.loading = true;

    this.categoriaService
      .updateCategoria(this.categoriaInput.id, categoria)
      .subscribe((data: any) => {
        this.categoria = data;
        //console.log(valor)
        this.loading = false;
        if (data) {

          Swal.fire({
            title: 'Información actualizada',
            text: '',
            icon: 'success',
            allowOutsideClick: false,
          }).then((r) => {
            this.closeModal()

            //window.location.reload();
           // this.dialogRef.close(data);
          });
        } else {
          Swal.fire({
            title: 'No se pudo actualizar la información',
            text: 'Revisa los campos',
            icon: 'error',
            allowOutsideClick: false,
          }).then((r) => {});
        }
      });
  }


  closeModal(){

    this.modal.dismissAll();
  }
}
