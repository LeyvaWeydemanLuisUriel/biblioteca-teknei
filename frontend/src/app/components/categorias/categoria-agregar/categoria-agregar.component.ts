import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Categoria } from 'src/app/interfaces/categoria.interface';
import { CategoriaService } from 'src/app/services/categoria.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-categoria-agregar',
  templateUrl: './categoria-agregar.component.html',
  styleUrls: ['./categoria-agregar.component.css']
})
export class CategoriaAgregarComponent implements OnInit {

  loading = false;
  constructor(private categoriaService:CategoriaService,public activeModal: NgbActiveModal ) { 
    
  }

  ngOnInit(): void {
  }
  crear(categoria:Categoria): void {
    //console.log(libro);
    this.loading=true;
    this.categoriaService.addCategoria(categoria).subscribe((data:any) =>{
       // console.log(data)
        if(data){
          //this.loading=false;

          Swal.fire({title:'Información guardada con éxito.',text:'',icon:'success',allowOutsideClick:false}).then(
            ()=>{
              //this.dialogRef.close(null); 
              this.closeModal();

            }
          )
          this.loading=false;

                }else{
                 // this.loading=false;
                 this.loading=false;

          Swal.fire({title:'No se pudo guardar la información.',text:'',icon:'error',allowOutsideClick:false})

        }
      })
   

  }
 
  closeModal() {
    this.activeModal.close(null);
  }
}
