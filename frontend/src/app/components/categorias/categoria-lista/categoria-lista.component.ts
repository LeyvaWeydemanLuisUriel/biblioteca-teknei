import { Component, OnInit, TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Categoria } from 'src/app/interfaces/categoria.interface';
import { CategoriaService } from 'src/app/services/categoria.service';
import Swal from 'sweetalert2';
import { CategoriaAgregarComponent } from '../categoria-agregar/categoria-agregar.component';
import { CategoriaEditarComponent } from '../categoria-editar/categoria-editar.component';
import { CategoriaVerComponent } from '../categoria-ver/categoria-ver.component';

@Component({
  selector: 'app-categoria-lista',
  templateUrl: './categoria-lista.component.html',
  styleUrls: ['./categoria-lista.component.css']
})
export class CategoriaListaComponent implements OnInit {
  categoria:Categoria;
  categoriasLista = [];
  loading = false;
  constructor(private categoriaService:CategoriaService, public modalService:NgbModal) { }

  ngOnInit(): void {
   this.loadCategorias();
  }
 loadCategorias() {
    this.loading = true;
    this.categoriaService
      .getCategorias()
      .subscribe((data) => {
        //console.log(data)

        if (data.length != 0) {
          //console.log(data.filter(x=>x.obligacion !=null));
          this.categoriasLista = data;
          this.loading = false;
          // this.onResize();
        } else {
          
          this.loading = false;

          Swal.fire({
            title:
              'Sin categorias registradas',
            text: '',
            icon: 'error',
            allowOutsideClick: false,
          }).then((r) => {
            // window.location.reload();
          });
        }
      });
  }

  agregarInformacion() {
    this.modalService.open(CategoriaAgregarComponent).result.then((result) => {
      console.log(result);
      if(result==null){
        this.loadCategorias();
      }
    }, (reason) => {
      console.log(reason);
    });
    

  }

  editar(template: TemplateRef<any>,cate:Categoria) {
    this.categoria = cate;

    this.modalService.open(template).result.then((result) => {
      if(!result){
        this.loadCategorias();

      }
    }, (reason) => {
      if(!reason)
      this.loadCategorias();

    });
   
  }

  ver(template: TemplateRef<any>,cate:Categoria){

    this.categoria = cate;
    this.modalService.open(template).result.then((result) => {
      this.loadCategorias();

      if(result==null){
        this.loadCategorias();
      }
    }, (reason) => {
      console.log(reason);
    });

  }

  borrar(element) {
    console.log(element)
    Swal.fire({
      title: '¿Estás seguro de eliminar la categoría ' + element.nombre + '?',
      text: '¡Esta acción no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.loading = true;
        this.categoriaService
          .deleteCategoria(element.id)
          .subscribe((data) => {
            if (data.message == 'Eliminado') {
              this.loading = false;
              Swal.fire({
                title: 'Registro eliminado',
                text: 'Has eliminado el registro correctamente',
                icon: 'success',
                allowOutsideClick: false,
              }).then((result) => {
                if (result.isConfirmed) {
                 this.loadCategorias();
                }
              });

              //this.loading = false;
            } else {
              this.loading = false;

              Swal.fire({
                title: 'No pudo eliminarse',
                text: '',
                icon: 'error',
                allowOutsideClick: false,
              });
            }
          });
      }
    });
  }
}
