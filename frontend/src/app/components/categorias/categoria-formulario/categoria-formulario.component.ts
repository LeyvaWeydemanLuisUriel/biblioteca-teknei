import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Categoria } from 'src/app/interfaces/categoria.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-categoria-formulario',
  templateUrl: './categoria-formulario.component.html',
  styleUrls: ['./categoria-formulario.component.css']
})
export class CategoriaFormularioComponent implements OnInit {

  @Input() categoria:Categoria;
  @Output() submitForm: EventEmitter<any> = new EventEmitter();
  public categoriaForm: FormGroup;
  public submit: boolean;

  constructor(private fb: FormBuilder) { 
  }

  ngOnInit() {

    this.setFormulario();
    this.enEdicion();
  }

  setFormulario(): void {
    this.categoriaForm = this.fb.group({
      id: [''],
      nombre: ['', [Validators.required,Validators.maxLength(20)]],
      descripcion: ['', [Validators.required,Validators.maxLength(100)]],
    });
  }
  get f() {
    //this.categoriaForm.controls['nombre'].hasError('maxlength')
    console.log(    this.categoriaForm.controls['nombre'].hasError('maxlength')
    )
    return this.categoriaForm.controls;
  }
  private enEdicion(): void {

    if (this.categoria) {
      let { nombre, descripcion } = this.categoria;

      this.categoriaForm.patchValue({
        
        nombre,
        descripcion
      });
    }
    
  }

  onSubmit(): void {
    this.submit = true;
    if (!this.categoriaForm.valid){
      Swal.fire('Complete los datos requeridos', '', 'error');


    //  this.irAtras.next(true);

     } else{
      
      let datos = new FormData();
      datos.append(
        'nombre',
         this.categoriaForm.get('nombre')?.value
      );
      datos.append(
        'descripcion',
         this.categoriaForm.get('descripcion')?.value
      );

      this.submitForm.emit(datos);}
  }

  borrarFormulario(): void {   
      this.categoriaForm.reset();
     
  }


}
