import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Libro } from '../interfaces/libro.interface';

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  libroURL = environment.apiURL + 'libros';

  /**
   * Cabeceras que permiten al cliente y al servidor
   * enviar información junto la petición o respuesta.
   */
   httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient) { }

  getLibro(id:number): Observable<any> {
    return this.http.get(this.libroURL +'/'+ id);
  }

  getLibros(): Observable<any> { 
    return this.http.get(this.libroURL);
  }

  addLibro(data:Libro):Observable<any> {    
    return this.http.post(this.libroURL,data);
  }

  updateLibro(id:number,data:Libro):Observable<any> {    
    return this.http.post(this.libroURL+ '/update/'+id,data);
  }

  deleteLibro(id:number):Observable<any> {    
    return this.http.delete(this.libroURL+ '/delete/'+id);
  }


}
