import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Categoria } from '../interfaces/categoria.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  categoriaURL = environment.apiURL + 'categorias';

  /**
   * Cabeceras que permiten al cliente y al servidor
   * enviar información junto la petición o respuesta.
   */
   httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient) { }

  getCategoria(id:number): Observable<any> {
    return this.http.get(this.categoriaURL +'/'+ id);
  }

  getCategorias(): Observable<any> {    
    return this.http.get(this.categoriaURL);
  }

  addCategoria(data:Categoria):Observable<any> {    
    return this.http.post(this.categoriaURL,data);
  }

  updateCategoria(id:number,data:Categoria):Observable<any> {    
    return this.http.post(this.categoriaURL+ '/update/'+id,data);
  }

  deleteCategoria(id:number):Observable<any> {    
    return this.http.delete(this.categoriaURL+ '/delete/'+id);
  }
}
