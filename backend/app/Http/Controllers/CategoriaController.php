<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  Categoria::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria   = new Categoria();
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;

        $categoria->save();

        return $categoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return Categoria::where('id', $id)->first();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request,$id)
    {
        $categoria   = Categoria::findOrFail($id);
        if ($categoria) {
            
            $categoria->update($request->all());

            return $categoria;
        } else {
            return null;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $categoria = Categoria::findOrFail($id);
        //return $categoria;
        if ($categoria) {

            $categoria->delete();
            return  response()->json(['message' => 'Eliminado']);
            //echo 'The file ' . $filename . ' was deleted successfully!';



        } else {
            return  response()->json(['message' => 'Error']);
            // echo 'There was a error deleting the file ' . $filename;
        }
    }
}
