<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Libro;
use Illuminate\Http\Request;

class LibroController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libro = Libro::all();
        foreach ($libro as $value) {
            $cate = Categoria::where('id',$value->categorias_id)->first();
            $value->categoria = $cate->nombre;

        }
        return $libro;
    }

     
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libro   = new Libro();
        $libro->titulo = $request->titulo;
        $libro->autor = $request->autor;
        $libro->categorias_id = $request->categorias_id;
        if ($request->has('portada')) {
            $file = $request->file('portada');

            if ($file) {
                $name =  $file->getClientOriginalName();
                $file->move(public_path(  'portadas-libros/' . $request->titulo), $name);
                $libro->portada = 'portadas-libros/' . $request->titulo . '/' . $name;
            } else {

                $libro->portada  = null;
            }
        }

        $libro->save();

       return $libro;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Libro::where('id', $id)->first();
        
       
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $libro   = Libro::findOrFail($id);
        if($libro){
            //return $request->all();

            $libro->titulo = $request->titulo;
            $libro->autor = $request->autor;
            //$libro->categorias_id = $request->categorias_id;
            if ($request->has('portada')) {
                
                $file = $request->file('portada');
    
                if ($file) {
                    $name =  $file->getClientOriginalName();
                    $file->move(public_path(  'portadas-libros/' . $request->titulo), $name);
                    $libro->portada = 'portadas-libros/' . $request->titulo . '/' . $name;
                } else {
    
                    //$libro->portada  = null;
                }
                $libro->save();
                return $libro;

            }
    
    
       
        }else{
            return null; 
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $libro = Libro::findOrFail($id);
        if($libro){
            if (unlink($libro->portada)) {
                $libro->delete();
                return  response()->json(['message' => 'Eliminado']);
                //echo 'The file ' . $filename . ' was deleted successfully!';
            } else {
                return  response()->json(['message' => 'Error']);
                // echo 'There was a error deleting the file ' . $filename;
            }

        }
    }

    public function getFile(Request $request)
    {
        return response()->file(public_path() . '/' . $request->url);
    }

}
