<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;

    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['titulo','autor','categorias_id','portada'];
    public function getTituloAttribute($value)
    {
        return strtoupper($value);
    }

    public function getAutorAttribute($value)
    {
        return strtoupper($value);
    }
    

    public function categoriass()
    {
        return $this->belongsTo('App\Models\Categoria','categorias_id','id');
    }
}
