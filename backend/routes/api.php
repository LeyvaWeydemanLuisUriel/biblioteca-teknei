<?php

use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\LibroController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('archivo', [LibroController::class, 'getFile']);
Route::delete('categorias/delete/{id}',[CategoriaController::class,'delete']);
Route::post('categorias/update/{id}',[CategoriaController::class,'update']);
Route::delete('libros/delete/{id}',[LibroController::class,'delete']);
Route::post('libros/update/{id}',[LibroController::class,'update']);
Route::resource('libros', LibroController::class)->only(['store','show','index']);
Route::resource('categorias', CategoriaController::class)->only(['store','show','index']);





